package fr.atnock.pierre_emmanuel.starwars;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.swapi.sw.StarWarsApi;
import com.swapi.models.*;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    private Button morePlanetButton;
    private PlanetAdapter planetsAdapter;
    private ProgressBar loader;
    private ListView planets;
    private TextView noSWAPI;

    private int nb_page;
    private int nb_planet;
    private final List<Planet> listOfPlanets = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        planets = findViewById(R.id.planet);
        loader = findViewById(R.id.loader);
        noSWAPI = findViewById(R.id.noSWAPIFound);
        nb_page = 1;
        nb_planet = -1;

        planetsAdapter = new PlanetAdapter(this, listOfPlanets, planetSelectedListener);
        planets.setAdapter(planetsAdapter);

        morePlanetButton = findViewById(R.id.morePlanet);
        morePlanetButton.setOnClickListener(morePlanetButtonClicked); // change the listener

        StarWarsApi.init();
        StarWarsApi.getApi().getAllPlanets(nb_page, callback_swapi);
        morePlanetButton.setEnabled(false);


    }


    private final Callback<SWModelList<Planet>> callback_swapi = new Callback<SWModelList<Planet>>() {
        @Override
        public void success(SWModelList<Planet> simple, Response response) {
            if(nb_planet < 0) {
                nb_planet = simple.count;
            }
            Log.i("NB_PLANET BEFORE : ", ""+nb_planet);
            nb_planet -= simple.results.size();
            Log.i("NB_PLANET AFTER : ", ""+nb_planet);
            if(nb_planet == 0) {
                morePlanetButton.setEnabled(false);
                morePlanetButton.setText("Toutes les planètes sont affichées");
            } else {
                morePlanetButton.setEnabled(true);
            }

            for(int i = 0; i < simple.results.size(); i++) {
                listOfPlanets.add(simple.results.get(i));
            }

            planetsAdapter.notifyDataSetChanged();
            loader.setVisibility(View.GONE);
        }

        @Override
        public void failure(RetrofitError error) {
            noSWAPI.setVisibility(View.VISIBLE);
            loader.setVisibility(View.GONE);
        }


    };

    private final PlanetAdapter.OnPlanetSelectedListener planetSelectedListener = new PlanetAdapter.OnPlanetSelectedListener() {
        @Override
        public void handle(final Planet planet) {
            startActivity(PlanetDetailActivity.getStartIntent(MainActivity.this, planet));
            setResult(RESULT_OK);
        }
    };

    private final View.OnClickListener morePlanetButtonClicked = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
            StarWarsApi.getApi().getAllPlanets(++nb_page, callback_swapi);
            morePlanetButton.setEnabled(false);
        }
    };
}
