package fr.atnock.pierre_emmanuel.starwars;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import com.swapi.models.*;

/**
 * Device adapter
 */
public class PlanetAdapter extends ArrayAdapter<Planet> {

    /**
     * Declare an inner interface to listen click event on device items
     */
    public interface OnPlanetSelectedListener {
        void handle(final Planet device);
    }

    private final OnPlanetSelectedListener onPlanetSelectedListener;

    PlanetAdapter(@NonNull final Context context, final List<Planet> planets, final OnPlanetSelectedListener listener) {
        super(context, R.layout.planet_list_item, planets);
        onPlanetSelectedListener = listener;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable final View convertView, @NonNull final ViewGroup parent) {
        View holder = convertView;
        if (convertView == null) {
            final LayoutInflater vi = LayoutInflater.from(getContext());
            holder = vi.inflate(R.layout.planet_list_item, null);
        }

        final Planet planet = getItem(position);
        if (planet == null) {
            return holder;
        }

        // display the name
        final TextView deviceName = holder.findViewById(R.id.planetName);
        if (deviceName != null) {
            deviceName.setText(planet.name);
        }

        // When this device item is clicked, trigger the listener
        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (onPlanetSelectedListener != null) {
                    onPlanetSelectedListener.handle(planet);
                }
            }
        });

        return holder;
    }
}