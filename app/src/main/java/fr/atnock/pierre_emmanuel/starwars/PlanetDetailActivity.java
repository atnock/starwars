package fr.atnock.pierre_emmanuel.starwars;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.swapi.models.Planet;

public class PlanetDetailActivity extends AppCompatActivity {

    static private Planet planet;

    public static Intent getStartIntent(final Context ctx, final Planet received_planet) {
        planet = received_planet;
        return new Intent(ctx, PlanetDetailActivity.class);
    }

    private TextView planet_name;
    private TextView planet_rotation_period;
    private TextView planet_orbital_period;
    private TextView planet_diameter;
    private TextView planet_climate;
    private TextView planet_surface_water;
    private TextView planet_population;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planet_detail);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        initUI();
    }

    /**
     * Setup the UI and bind listeners
     */
    private void initUI() {
        planet_name = findViewById(R.id.planet_name);
        planet_rotation_period = findViewById(R.id.planet_rotation_period);
        planet_orbital_period = findViewById(R.id.planet_orbital_period);
        planet_diameter = findViewById(R.id.planet_diameter);
        planet_climate = findViewById(R.id.planet_climate);
        planet_surface_water = findViewById(R.id.planet_surface_water);
        planet_population = findViewById(R.id.planet_population);

        planet_name.setText(planet.name);
        planet_rotation_period.setText(planet.rotationPeriod);
        planet_orbital_period.setText(planet.orbitalPeriod);
        planet_diameter.setText(planet.diameter);
        planet_climate.setText(planet.climate);
        planet_surface_water.setText(planet.surfaceWater);
        planet_population.setText(planet.population);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
